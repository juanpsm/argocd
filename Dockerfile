FROM argoproj/argocd:v2.1.1

USER root
ARG SOPS_VERSION="v3.7.1"
ARG AGE_VERSION="v1.0.0"
ARG HELMFILE_VERSION="v0.140.1"

COPY helm-wrapper.sh /usr/local/bin

RUN apt-get update && \
    apt-get install -y curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    curl -o /usr/local/bin/sops -L https://github.com/mozilla/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux && \
    chmod +x /usr/local/bin/sops && \
    curl -L https://github.com/FiloSottile/age/releases/download/${AGE_VERSION}/age-${AGE_VERSION}-linux-amd64.tar.gz | \
    tar -xz -C /tmp/  && \
    mv /tmp/age/age /usr/local/bin/age && \
    chmod +x /usr/local/bin/age && \
    curl -L http://raw.githubusercontent.com/travisghansen/argo-cd-helmfile/master/src/argo-cd-helmfile.sh -o /usr/local/bin/argo-cd-helmfile.sh && \
    chmod +x /usr/local/bin/argo-cd-helmfile.sh && \
    curl -L http://github.com/roboll/helmfile/releases/download/${HELMFILE_VERSION}/helmfile_linux_amd64 -o /usr/local/bin/helmfile && \
    chmod +x /usr/local/bin/helmfile && \
    cd /usr/local/bin && \
    mv helm helm.bin && \
    mv helm2 helm2.bin && \
    mv helm-wrapper.sh helm && \
    ln helm helm2 && \
    chmod +x helm helm2

USER argocd

ARG HS_PLUGIN_VERSION="3.8.3"
ARG HS_PLUGIN_REPO="https://github.com/jkroepke/helm-secrets"
ARG HG_PLUGIN_VERSION="0.10.0"
ARG HG_PLUGIN_REPO="https://github.com/aslafy-z/helm-git"

RUN helm plugin install ${HS_PLUGIN_REPO} --version ${HS_PLUGIN_VERSION} && \
    helm plugin install ${HG_PLUGIN_REPO} --version ${HG_PLUGIN_VERSION}

ENV HELM_PLUGINS="/home/argocd/.local/share/helm/plugins/"

